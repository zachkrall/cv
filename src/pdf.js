const PDFDocument = require('pdfkit')
const fs = require('fs')
const path = require('path')

const margin = 30

class PDF {
  constructor({
    filename,
    margins={
      left: margin,
      top: margin,
      right: margin,
      bottom: margin
    }
  }={}){

    // init PDF doc
    this.doc = new PDFDocument({
      margins: margins
    })
    // set filename
    this.filename = filename

    this.doc.pipe(fs.createWriteStream(filename))

    this.textColor = '#000000'
    this.fontSize = 12
  }

  title(t){
    this
      .doc
      .fontSize(this.fontSize)
      .fillColor('#999999')
      .text(t + '\n\n\n')
  }

  measuredText(t){
    this
      .doc
      .fontSize(this.fontSize)
      .fillColor(this.textColor)
      .text(t + '\n\n\n', {
        width: 360,
        align: 'left'
      })
  }

  h1(t){
    this
      .doc
      .fontSize(this.fontSize)
      .fillColor(this.textColor)
      .text(t.toUpperCase() + '\n\n')
  }

  text(t){
    this
      .doc
      .fontSize(12)
      .fillColor(this.textColor)
      .text(t)
  }

  done(){
    this.doc.end()
  }
}

module.exports = PDF
