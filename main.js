const path = require('path')
const PDF = require('./src/pdf')
const cv = require('./data/cv.json')

let doc = new PDF({
  filename: path.resolve(__dirname, 'dist', 'zach-krall_cv.pdf')
})

doc.title('Curriculum Vitae')

doc.measuredText('Zach Krall (American, b. 1993, Japan) is a computational artist based in New York City.')


// <div class="pb4" v-for="(content, heading) in cv" :key="title">
Object.keys(cv).forEach( heading =>{
  doc.h1(heading)

  Object.keys(cv[heading]).forEach( year => {
    doc.text(year)

    Object.keys(cv[heading][year]).forEach( entry => {
      doc.text(cv[heading][year][entry])
    })
  })
})


//   <h2 class="f5 ttu normal mt0">{{ heading }}</h2>
//   <div v-for="(items, key) in content" :key="key">
//     <div
//       v-for="(line, year) in items" :key="title"
//       class="flex"
//     >
//       <h3 class="normal v-top f5 flex-shrink-0">{{ year }}</h3>
//       <ul class="v-top">
//         <li
//           v-for="(entry, index) in line"
//           :key="index"
//           v-html="entry"
//           class="list pb2 indent"
//         ></li>
//       </ul>
//     </div>
//   </div>
// </div>
// </div>

// always at the end
doc.done()
